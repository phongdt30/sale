<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Article;
use \App\Category;
use Gate;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(!Gate::allows('isAdmin')){
            abort(404,"Sorry, You can do this actions");
        }
        
        $articles = Article::paginate(10);
        $category = Category::all()->pluck('title', 'id');


        return view('article.index',compact('articles', 'category'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        Article::create($request->all());

        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $requestData = $request->all();
        $article = Article::findOrFail($request->id);

        $imageName = $request->id . '.' . $request->file('image')->getClientOriginalExtension();

        $request->file('image')->move(
            base_path() . '/public/images/catalog/', $imageName
        );

  
        // $request->image = '/public/images/catalog/'. $imageName;

        $requestData['image'] = '/images/catalog/'. $imageName;

        $article->update($requestData);
       
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $article = Article::findOrFail($request->id);
        $article->delete();

        return back();

    }
}
