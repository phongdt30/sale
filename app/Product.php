<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $fillable = ['id', 'slug', 'category_id', 'name', 'date', 'description', 'details', 'price', 'image', 'extras', 'features', 'meta_title', 'meta_description', 'meta_keywords'];
}
