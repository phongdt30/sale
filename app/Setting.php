<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //

    protected $fillable = ['id', 'key', 'name', 'descriptio', 'value', 'field', 'active'];
}
