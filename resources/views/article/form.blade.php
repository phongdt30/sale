	        	<div class="form-group">
		        	<label for="title">Title</label>
		        	<!-- <input type="text" class="form-control" name="title" id="title"> -->
		        	{!! Form::text('title', '', ['class' => 'form-control', 'id' => 'title'] ); !!}
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Category</label>
	        		<!-- <input type="text" name="category_id" id="category_id" class="form-control"> -->
	        		{!! Form::select('category_id', $category, null, ['id' => 'category_id', 'class' => 'form-control']) !!}
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Slug</label>
	        		<!-- <input type="text" name="slug" id="slug" class="form-control"> -->
	        		{!! Form::text('slug', '', ['class' => 'form-control', 'id' => 'slug'] ); !!}
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Content</label>
	        		<!-- <textarea type="text" name="content" id="content" class="form-control textarea" cols="20" rows="5"></textarea> -->
	        		{!! Form::textarea('content', '', ['class' => 'form-control textarea', 'id' => 'content'] ) !!}
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Image</label>
	        		
	        		<!-- <input type="file" name="image" id="image" value=""> -->
	        		{!!  Form::file('image')  !!}
	        	</div>


	        	<div class="form-group">
	        		<label for="des">Date</label>
	        		<!-- <input type="text" name="date" id="date"  data-provide="datepicker" class="form-control datetimepicker"> -->
	        		{!!  Form::text('date', '' , ['class' => 'form-control datetimepicker', 'id' => 'date'] )  !!}
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Featured</label>
	        		<!-- <input type="checkbox" value="1" name="featured" id="featured"> -->
	        		{!!  Form::checkbox('featured', '1' , ['class' => 'form-control', 'id' => 'featured'] )  !!}
	        	</div>
	        	


	        	<div class="form-group">
	        		<label for="des">Status</label>
	        		{!!  Form::select('status', ['PUBLISHED' => 'PUBLISHED', 'DRAFT' => 'DRAFT'], '', ['class' => 'form-control', 'id' => 'status'] )  !!}
	        	<!-- 	<select class="form-control" id="status" name="status">
                        <option value="PUBLISHED">PUBLISHED</option>
                        <option value="DRAFT">DRAFT</option>
                      
                    </select>
 -->
	        	</div>

