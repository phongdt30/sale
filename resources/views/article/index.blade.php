@extends('layouts.master')

@section('content')


	<div class="">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">All</h3>
			</div>

			<div class="box-body">
				<table class="table table-responsive">
					<thead>
						<tr>
							<th>Name</th>
							<th>Status</th>
							<th>Modify</th>
						</tr>
						
					</thead>

					<tbody>

						@foreach($articles as $item)
							<tr>
								<td>{{$item->title}}</td>
								<td>{{$item->status}}</td>
								<td>
									<button class="btn btn-info" data-title="{{$item->title}}"  data-status="{{$item->status}}" data-category-id="{{$item->category_id}}" data-slug="{{$item->slug}}" data-content="{{$item->content}}" data-image="{{$item->image}}" data-date="{{$item->date}}" data-featured="{{$item->featured}}" data-id="{{$item->id}}" data-toggle="modal" data-target="#edit">Edit</button>
									/
									<button class="btn btn-danger" data-id={{$item->id}} data-toggle="modal" data-target="#delete">Delete</button>
								</td>
							</tr>

						@endforeach
					</tbody>


				</table>	
				{{ $articles->links() }}			
			</div>
		</div>
	</div>



	<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
 	Add New
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New</h4>
      </div>
      <form action="{{route('article.store')}}" method="post" enctype="multipart/form-data">
 
      		{{csrf_field()}}
	      <div class="modal-body">
				@include('article.form')
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save</button>
	      </div>
 
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit </h4>
      </div>
      <!-- <form action="{{route('article.update','test')}}" method="post"> -->
     <form action="{{route('article.destroy','test')}}" method="post" enctype="multipart/form-data">
      		{{method_field('patch')}}
      		{{csrf_field()}}
	      <div class="modal-body">
	      		<input type="hidden" name="id" id="id" value="">
				@include('article.form')
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save Changes</button>
	      </div>
      
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Delete Confirmation</h4>
      </div>
      <form action="{{route('article.destroy','test')}}" method="post" enctype="multipart/form-data">
      		{{method_field('delete')}}
      		{{csrf_field()}}
	      <div class="modal-body">
				<p class="text-center">
					Are you sure you want to delete this?
				</p>
	      		<input type="hidden" name="id" id="id" value="">

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-success" data-dismiss="modal">No, Cancel</button>
	        <button type="submit" class="btn btn-warning">Yes, Delete</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script>

  
  $('#edit').on('show.bs.modal', function (event) {

      var button 	= $(event.relatedTarget) 
      var title 	= button.data('title') 
      var category_id 	= button.data('category-id')
      var slug 	= button.data('slug') 
      var content 	= button.data('content') 
      var image 	= button.data('image') 
      var date 	= button.data('date') 
      var featured 	= button.data('featured') 
      var status 	= button.data('status') 
      var id 		= button.data('id') 
      var modal 	= $(this)

      modal.find('.modal-body #title').val(title);
      modal.find('.modal-body #category_id').val(category_id);
      modal.find('.modal-body #slug').val(slug);
      modal.find('.modal-body #content').val(content);
      modal.find('.modal-body #image').val(image);
      modal.find('.modal-body #date').val(date);

      // modal.find('.modal-body #featured').val(featured);
      if(featured == '1'){

      	 modal.find('.modal-body #featured').attr('checked','checked');
      }
      
      modal.find('.modal-body #status').val(status);
      modal.find('.modal-body #id').val(id);
})


  $('#delete').on('show.bs.modal', function (event) {

      var button = $(event.relatedTarget) 

      var id = button.data('id') 
      var modal = $(this)

      modal.find('.modal-body #id').val(id);
})

$(function() {
	
	$(".datetimepicker").datepicker({ 

		dateFormat: 'yy-mm-dd'
	});

});
</script>
@endsection

