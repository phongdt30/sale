	        	<div class="form-group">
		        	<label for="title">Title</label>
		        	<input type="text" class="form-control" name="name" id="name">
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Category</label>
	        		<input type="text" name="category_id" id="category_id" class="form-control">
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Description</label>
	        		<textarea type="text" name="description" id="description" class="form-control textarea" cols="20" rows="5"></textarea>
	        	</div>


	        	<div class="form-group">
	        		<label for="des">Slug</label>
	        		<input type="text" name="slug" id="slug" class="form-control">
	        	</div>

	        	<div class="form-group">
		        	<label for="title">Price</label>
		        	<input type="text" class="form-control" name="price" id="price">
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Details</label>
	        		<textarea type="text" name="details" id="details" class="form-control textarea" cols="20" rows="5"></textarea>
	        	</div>

	        	<div class="form-group">
	        		<label for="des">Image</label>
	        		
	        		<input type="file" name="image" id="image" value="">
	        	</div>


	        	<div class="form-group">
	        		<label for="des">Date</label>
	        		<input type="text" name="date" id="date"  data-provide="datepicker" class="form-control datetimepicker">

	        	</div>

	        	<div class="form-group">
	        		<label for="des">Featured</label>
	        		<input type="checkbox" value="1" name="features" id="features">
	        	</div>
	        	


	        	<div class="form-group">
	        		<label for="des">Status</label>
	        		
	        		<select class="form-control" id="status" name="status">
                        <option value="PUBLISHED">PUBLISHED</option>
                        <option value="DRAFT">DRAFT</option>
                      
                    </select>

	        	</div>

