@extends('layouts.master')

@section('content')


	<div class="">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">All</h3>
			</div>

			<div class="box-body">
				<table class="table table-responsive">
					<thead>
						<tr>
							<th>Name</th>
							<th>Status</th>
							<th>Modify</th>
						</tr>
						
					</thead>

					<tbody>

						@foreach($datas as $item)
							<tr>
								<td>{{$item->name}}</td>
								<td>{{$item->status}}</td>
								<td>
									<button class="btn btn-info" data-name="{{$item->name}}"  data-status="{{$item->status}}" data-category-id="{{$item->category_id}}" data-slug="{{$item->slug}}" data-description="{{$item->description}}" data-details="{{$item->details}}" data-price="{{$item->price}}" data-image="{{$item->image}}" data-extras="{{$item->extras}}" data-price="{{$item->price}}" data-date="{{$item->date}}" data-features="{{$item->features}}" data-id="{{$item->id}}" data-toggle="modal" data-target="#edit">Edit</button>
									/
									<button class="btn btn-danger" data-id={{$item->id}} data-toggle="modal" data-target="#delete">Delete</button>
								</td>
							</tr>

						@endforeach
					</tbody>


				</table>	
				{{ $datas->links() }}			
			</div>
		</div>
	</div>



	<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
 	Add New
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New</h4>
      </div>
      <form action="{{route('product.store')}}" method="post">
      		{{csrf_field()}}
	      <div class="modal-body">
				@include('product.form')
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit </h4>
      </div>
      <form action="{{route('product.update','test')}}" method="post">
      		{{method_field('patch')}}
      		{{csrf_field()}}
	      <div class="modal-body">
	      		<input type="hidden" name="id" id="id" value="">
				@include('product.form')
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save Changes</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Delete Confirmation</h4>
      </div>
      <form action="{{route('product.destroy','test')}}" method="post">
      		{{method_field('delete')}}
      		{{csrf_field()}}
	      <div class="modal-body">
				<p class="text-center">
					Are you sure you want to delete this?
				</p>
	      		<input type="hidden" name="id" id="id" value="">

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-success" data-dismiss="modal">No, Cancel</button>
	        <button type="submit" class="btn btn-warning">Yes, Delete</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script>

  
  $('#edit').on('show.bs.modal', function (event) {

      var button 	= $(event.relatedTarget) 
      var name 	= button.data('name') 
      var category_id 	= button.data('category-id')
      var slug 	= button.data('slug') 
      var details 	= button.data('details')
      var description   = button.data('description')
      var price   = button.data('price')
        
      var image 	= button.data('image') 
      var date 	= button.data('date') 
      var features 	= button.data('features') 
      var status 	= button.data('status') 
      var id 		= button.data('id') 
      var modal 	= $(this)

      modal.find('.modal-body #name').val(name);
      modal.find('.modal-body #category_id').val(category_id);
      modal.find('.modal-body #slug').val(slug);
      modal.find('.modal-body #details').val(details);
      modal.find('.modal-body #description').val(description);

      modal.find('.modal-body #slug').val(slug);

      modal.find('.modal-body #price').val(price);
      // modal.find('.modal-body #image').val(image);
      modal.find('.modal-body #date').val(date);

      // modal.find('.modal-body #features').val(features);
      if(features == '1'){

      	 modal.find('.modal-body #features').attr('checked','checked');
      }
      
      modal.find('.modal-body #status').val(status);
      modal.find('.modal-body #id').val(id);
})


  $('#delete').on('show.bs.modal', function (event) {

      var button = $(event.relatedTarget) 

      var id = button.data('id') 
      var modal = $(this)

      modal.find('.modal-body #id').val(id);
})

$(function() {
	
	$(".datetimepicker").datepicker({ 

		dateFormat: 'yy-mm-dd'
	});

});
</script>
@endsection

