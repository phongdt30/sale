<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('profile', function(){
    return view('profile');
});

/* View Composer*/
View::composer(['*'], function($view){
    $user = Auth::user();
    $view->with('user',$user);
});

Route::group(['middleware' => ['web']], function () {
    //Login Routes...
    Route::resource('admin/category','CategoryController');
	Route::resource('admin/theme','ThemeController');
	Route::resource('admin/widget','WidgetController');

	Route::resource('admin/article','ArticleController');

	Route::resource('admin/user','UserController');
	Route::resource('admin/setting','SettingController');

	Route::resource('admin/product','ProductController');

	Route::get('admin/file','FileController@index');
	Route::post('admin/file','Filecontroller@doUpload');


});